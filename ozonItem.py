from selenium.webdriver.common.by import By
import re

class OzonItem:
    def __init__(self, el):
        self.name = el.find_element(By.CSS_SELECTOR, "a.km").text
        self.priceArray = self.get_price_array(el)

    def get_price_array(self, el):
        rouble_string = " ₽"
        split_space_text = el.text.split(" ")
        text_with_rouble_array = list(filter(lambda e : rouble_string in e, split_space_text))
        text_with_rouble_string = " ".join(text_with_rouble_array)
        split_rouble_array = text_with_rouble_string.split(rouble_string)
        price_array = list(map(lambda e : self.get_number_from_digits_string(e), split_rouble_array))
        price_array = list(filter(lambda e : e, price_array))
        return price_array


    def get_number_from_digits_string(self, text):
        m = re.findall(r'\d+', text)
        return ''.join(m)