from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

from ozonItem import OzonItem

options = webdriver.ChromeOptions()
#options.add_argument("--disable-blink-features=AutomationControlled")
# options.add_argument('--headless')
# options.add_argument('--no-sandbox')
# options.add_argument('--disable-dev-shm-usage')
caps = DesiredCapabilities().CHROME
#caps["pageLoadStrategy"] = "normal"  #  complete
caps["pageLoadStrategy"] = "eager"  #  interactive

browser = webdriver.Chrome(chrome_options=options, desired_capabilities=caps)
browser.get('https://www.ozon.ru/my/favorites/shared?list=3djF0XC0b_f8v996XAjJJyYLu1V8KTIRXHBn75MLYEs')
item_el_list = browser.find_elements(By.CSS_SELECTOR, '.widget-search-result-container:first-child .p4k')
ozon_item_list = []
for itemEl in item_el_list:
    ozon_item = OzonItem(itemEl)
    ozon_item_list.append(ozon_item)
browser.quit()